/* global $ */

// Prevent FOUC
$(document).ready(() => {
    $('html').addClass('elements-loaded');
});
