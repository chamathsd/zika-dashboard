/* global $, ol, olcs */

let olMap;
let reportLayer;
const geoserverURL = 'http://localhost:8080/geoserver/wms';

// Logic to query report info from server and add it to flat map
function addReportInfoToMap() {
    olMap.removeLayer(reportLayer);

    reportLayer = new ol.layer.Tile({
        source: new ol.source.TileWMS(({
            url: geoserverURL,
            params: {
                LAYERS: 'zika:location',
                TILED: true,
            },
        })),
    });

    // Add report layer to the map
    olMap.addLayer(reportLayer);
}

$(document).ready(() => {
    // Map view setup
    // OpenStreetMaps Layer
    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM({
            // url: 'http://{a-c}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png',
            url: 'https://{a-b}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
            // url: 'http://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
            crossOrigin: null,
        }),
        visible: true,
    });

    osmLayer.on('postcompose', (evt) => {
        const ctx = evt.context;
        ctx.globalCompositeOperation = 'multiply';
        ctx.fillStyle = '#BBB';
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.globalCompositeOperation = 'source-over';
    });

    reportLayer = new ol.layer.Tile({
        source: new ol.source.TileWMS(({
            url: 'http://localhost:8080/geoserver/wms',
            params: {
                LAYERS: 'zika:location',
                TILED: true,
            },
        })),
    });


    // OpenLayers Map Object
    olMap = new ol.Map({
        target: 'viewContainer',
        layers: [
            osmLayer,
            reportLayer,
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([-53.30, -5.07]),
            zoom: 3,
        }),
    });

    addReportInfoToMap();

    olMap.on('click', (event) => {
        $('#reportHead').empty();
        $('#reportBody').empty();
        $('#reportHeader').text('');
        let count = 0;
        let featureLocation;
        olMap.forEachFeatureAtPixel(event.pixel, (feature) => {
            featureLocation = feature.get('location').split('-').join(' - ').replace('_', ' ');
            count += 1;
            $('#reportBody').append(`
                <tr>
                    <td data-label='Date' class="dark-content">${feature.get('reportDate')}</td>
                    <td data-label='Report Type' class="dark-content">${feature.get('dataField').split('_').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ')}</td>
                    <td data-label='Value' class="dark-content">${`${feature.get('value')} cases`}</td>
                </tr>`);
        });
        if (count > 0) {
            $('#reportHeader').text(`Reports for ${featureLocation}`);
            $('#reportHead').append(`
                <tr>
                  <th>Date</th>
                  <th>Report Type</th>
                  <th>Value</th>
                </tr>`);
        }
    });

    olMap.on('pointermove', (e) => {
        const pixel = olMap.getEventPixel(e.originalEvent);
        const hit = olMap.hasFeatureAtPixel(pixel);
        olMap.getViewport().style.cursor = hit ? 'pointer' : '';
    });


    // Globe view setup
    const ol3d = new olcs.OLCesium({ map: olMap, target: 'viewContainer' });
    ol3d.setEnabled(false);

    const scene = ol3d.getCesiumScene();
    scene.debugShowFramesPerSecond = true;
    scene.requestRenderMode = true;


    // Show map view by default - KEEP BELOW GLOBE INSTANTIATION
    // $('#mapContainer').hide();

    // View toggle handler
    $('[data-mobile-app-toggle] .button').click(function handleViewToggleClick() {
        $(this).siblings().removeClass('is-active');
        $(this).addClass('is-active');

        if ($(this).attr('id') === 'mapToggle') {
            if (ol3d.getEnabled()) {
                ol3d.setEnabled(false);
                $('#offCanvasLeft').foundation('close');
            }
        } else if (!ol3d.getEnabled()) {
            ol3d.setEnabled(true);
            $('#offCanvasLeft').foundation('close');
        }
    });

    // Filter trigger handlers
    $('#offCanvasLeft').on('opened.zf.offcanvas', () => {
        $('#filter-trigger-text').text('▼\xa0\xa0Filters\xa0\xa0▼');
    });

    $('#offCanvasLeft').on('closed.zf.offcanvas', () => {
        $('#filter-trigger-text').text('▲\xa0\xa0Filters\xa0\xa0▲');
    });
});
