package org.launchcode.zikaDashboard.data;

import org.launchcode.zikaDashboard.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Transactional
@Repository
public interface LocationRepository extends JpaRepository<Location, Integer> {

    List<Location> findById(int id);
    List<Location> findByName(String name);
    List<Location> findByHasReports(boolean hasReports);
}
