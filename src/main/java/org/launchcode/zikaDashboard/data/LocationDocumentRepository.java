package org.launchcode.zikaDashboard.data;

import org.elasticsearch.index.query.QueryBuilder;
import org.launchcode.zikaDashboard.models.es.LocationDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface LocationDocumentRepository
        extends ElasticsearchRepository<LocationDocument, String> {

    Iterable<LocationDocument> search(QueryBuilder query);

}
