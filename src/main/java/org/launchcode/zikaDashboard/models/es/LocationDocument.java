package org.launchcode.zikaDashboard.models.es;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import org.launchcode.zikaDashboard.geom.Feature;
import org.launchcode.zikaDashboard.geom.FeatureCollection;
import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Document(indexName = "#{esConfig.indexName}", type = "location")
public class LocationDocument {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

    @Field(type = FieldType.Integer)
    private Integer locationId;

    @Field(type = FieldType.Auto)
    @JsonSerialize(using = GeometrySerializer.class)
    private Geometry geometry;

    @Field(type = FieldType.Nested)
    private List<Report> reports = new ArrayList<>();

    public LocationDocument() {}

    public LocationDocument(Location location, List<Report> reports) {

        this.locationId = location.getId();
        this.geometry = location.getGeom();
        this.reports = reports;

    }

    public Integer getLocationId() {
        return locationId;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public List<Report> getReports() {
        return reports;
    }
}
