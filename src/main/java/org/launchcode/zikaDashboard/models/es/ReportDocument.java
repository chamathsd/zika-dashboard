package org.launchcode.zikaDashboard.models.es;

import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


//@Document(indexName = "#{esConfig.indexName}", type = "report")
public class ReportDocument {

//    @Id
//    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

    private String reportDate;
    private Integer locationId;
    private String dataField;
    private Integer value;
    private String unit;

    public ReportDocument() {}

    public ReportDocument(Report report, int locationId) {
        this.reportDate = report.getReportDate();
        this.locationId = locationId;
        this.dataField = report.getDataField();
        this.value = report.getValue();
        this.unit = report.getUnit();
    }

    public String getId() {
        return id;
    }

    public String getReportDate() {
        return reportDate;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public String getDataField() {
        return dataField;
    }

    public Integer getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }
}
