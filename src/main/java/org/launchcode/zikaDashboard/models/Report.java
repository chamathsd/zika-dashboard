package org.launchcode.zikaDashboard.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.*;


@Entity
@Table(name = "reports")
public class Report extends AbstractEntity {

    @Field(type = FieldType.Date, format = DateFormat.date)
    private String reportDate;

    private String location;
    private String locationType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    @JsonIgnore
    private Location locationData;

    private String dataField;
    private String dataFieldCode;
    private Integer value;
    private String unit;

    // Required by Hibernate
    public Report() {}

    public Report(String reportDate, String location, String locationType, String dataField, String dataFieldCode,
                  Integer value, String unit) {
        this.reportDate = reportDate;
        this.location = location;
        this.locationType = locationType;
        this.dataField = dataField;
        this.dataFieldCode = dataFieldCode;
        this.value = value;
        this.unit = unit;
    }

    public String getReportDate() {
        return reportDate;
    }

    public String getLocation() {
        return location;
    }

    public String getLocationType() {
        return locationType;
    }

    public Location getLocationData() {
        return locationData;
    }

    public String getDataField() {
        return dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public Integer getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }
}
