package org.launchcode.zikaDashboard.models;

import com.vividsolutions.jts.geom.Geometry;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


@Entity
@Table(name = "locations")
public class Location extends AbstractEntity {

    private String name;
    private String nameAlt;
    private String country;
    private String codeHasc;
    private Double latitude;
    private Double longitude;
    private Boolean hasReports;
    private Geometry geom;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "locationData")
    @Field(type = FieldType.Nested)
    private List<Report> reports;

    // Required by Hibernate
    public Location() {}

    public Location(String name, String nameAlt, String country,
                    String codeHasc, Double latitude, Double longitude,
                    Boolean hasReports, Geometry geom) {
        this.name = name;
        this.nameAlt = nameAlt;
        this.country = country;
        this.codeHasc = codeHasc;
        this.latitude = latitude;
        this.longitude = longitude;
        this.hasReports = hasReports;
        this.geom = geom;
    }

    public String getName() {
        return name;
    }

    public String getNameAlt() {
        return nameAlt;
    }

    public String getCountry() {
        return country;
    }

    public String getCodeHasc() {
        return codeHasc;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Boolean getHasReports() { return hasReports; }

    public Geometry getGeom() {
        return geom;
    }

    public List<Report> getReports() {
        return reports;
    }
}
