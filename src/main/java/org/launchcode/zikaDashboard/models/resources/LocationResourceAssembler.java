package org.launchcode.zikaDashboard.models.resources;

import org.launchcode.zikaDashboard.controllers.api.LocationRestController;
import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;


@Component
public class LocationResourceAssembler extends ResourceAssemblerSupport<Location, LocationResource> {

    @Autowired
    ReportResourceAssembler reportAssembler;

    public LocationResourceAssembler() {
        super(LocationRestController.class, LocationResource.class);
    }

    @Override
    public LocationResource toResource(Location location) {
        LocationResource resource = createResourceWithId(location.getId(), location);
        resource.uid = location.getId();
        resource.name = location.getName();
        resource.nameAlt = location.getNameAlt();
        resource.country = location.getCountry();
        resource.codeHasc = location.getCodeHasc();
        resource.latitude = location.getLatitude();
        resource.longitude = location.getLongitude();

        return resource;
    }
}
