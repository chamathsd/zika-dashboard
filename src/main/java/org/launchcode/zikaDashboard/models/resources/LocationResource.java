package org.launchcode.zikaDashboard.models.resources;

import com.vividsolutions.jts.geom.Geometry;
import org.launchcode.zikaDashboard.geom.FeatureCollection;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;


public class LocationResource extends ResourceSupport {

    public int uid;
    public String name;
    public String nameAlt;
    public String country;
    public String codeHasc;
    public Double latitude;
    public Double longitude;

}
