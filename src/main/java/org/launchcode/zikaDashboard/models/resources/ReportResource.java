package org.launchcode.zikaDashboard.models.resources;

import org.springframework.hateoas.ResourceSupport;


public class ReportResource extends ResourceSupport {

    public int uid;
    public String reportDate;
    public String location;
    public String dataField;
    //public String dataFieldCode;
    public int value;
    public String unit;

}

