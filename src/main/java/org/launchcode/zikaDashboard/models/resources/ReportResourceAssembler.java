package org.launchcode.zikaDashboard.models.resources;

import org.launchcode.zikaDashboard.controllers.api.ReportRestController;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;


@Component
public class ReportResourceAssembler extends ResourceAssemblerSupport<Report, ReportResource> {

    public ReportResourceAssembler() { super(ReportRestController.class, ReportResource.class); }

    @Override
    public ReportResource toResource(Report report) {
        ReportResource resource = createResourceWithId(report.getId(), report);
        resource.uid = report.getId();
        resource.reportDate = report.getReportDate();
        resource.location = report.getLocation();
        resource.dataField = report.getDataField();
        //resource.dataFieldCode = report.getDataFieldCode();
        if (report.getValue() != null) {
            resource.value = report.getValue();
        } else {
            resource.value = 0;
        }
        resource.unit = report.getUnit();

        return resource;
    }
}
