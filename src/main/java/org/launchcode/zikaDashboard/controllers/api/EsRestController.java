package org.launchcode.zikaDashboard.controllers.api;

import org.launchcode.zikaDashboard.data.LocationDocumentRepository;
import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.Report;
import org.launchcode.zikaDashboard.models.es.LocationDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.ElasticsearchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/api/es")
public class EsRestController {

    @Autowired
    private LocationDocumentRepository documentRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ReportRepository reportRepository;

    @PostMapping(value = "/refresh")
    public ResponseEntity refresh() {
        documentRepository.deleteAll();
        List<LocationDocument> documents = new ArrayList<>();
        List<Location> locations = locationRepository.findByHasReports(true);

        int count = 0;
        int added = 0;
        int total = locations.size();

        System.out.println("\nRefreshing Elasticsearch index...");
        for (Location location : locations) {
            List<Report> reports = reportRepository.findByLocationData_Id(location.getId());
            if (!reports.isEmpty()) {
                documents.add(new LocationDocument(location, reports));
                added++;
            }

            System.out.print("\rProcessing document " + ++count + "/" + total + "  ->  " + added + " added");
        }
        System.out.println();

        try {
            documentRepository.saveAll(documents);
        } catch (ElasticsearchException e) {
            Map<String, String> failedDocuments = e.getFailedDocuments();
            for (Map.Entry<String, String> entry : failedDocuments.entrySet()) {
                System.out.println(entry.getValue());
            }
            System.out.println();
        }

        return new ResponseEntity("Refreshed Elasticsearch index\n", HttpStatus.OK);
    }

}
