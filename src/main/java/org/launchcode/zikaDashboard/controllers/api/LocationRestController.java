package org.launchcode.zikaDashboard.controllers.api;

import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.launchcode.zikaDashboard.data.LocationDocumentRepository;
import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.geom.Feature;
import org.launchcode.zikaDashboard.geom.FeatureCollection;
import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.es.LocationDocument;
import org.launchcode.zikaDashboard.models.resources.LocationResource;
import org.launchcode.zikaDashboard.models.resources.LocationResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping(value = "/api/location")
public class LocationRestController {

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private LocationDocumentRepository documentRepository;

    @Autowired
    private LocationResourceAssembler assembler;

    @GetMapping
    public ResponseEntity getLocations() {
        List<LocationResource> resources = assembler.toResources(locationRepository.findAll());
        return new ResponseEntity(resources, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity getLocationById(@PathVariable("id") int id) {
        Location location = locationRepository.getOne(id);
        if(location == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        else {
            LocationResource resource = assembler.toResource(location);
            return new ResponseEntity(resource, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/geo")
    @ResponseBody
    public FeatureCollection getLocationGeometry(@RequestParam(name="id") Optional<Integer> id) {
        List<Location> locations;
        if (id.isPresent()) {
            locations = locationRepository.findById((int) id.get());
        } else {
            locations = locationRepository.findAll();
        }

        FeatureCollection featureCollection = new FeatureCollection();
        if (locations == null) return featureCollection;

        for (Location location : locations) {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("locationId", location.getId());
            properties.put("country", location.getCountry());
            properties.put("name", location.getName());

            featureCollection.addFeature(new Feature(location.getGeom(), properties));
        }

        return featureCollection;
    }

    @GetMapping(value = "es")
    public List<LocationDocument> getLocationsElastic() {
        MatchAllQueryBuilder query = QueryBuilders.matchAllQuery();
        List<LocationDocument> results = new ArrayList<>();

        Iterator<LocationDocument> iterator = documentRepository.search(query).iterator();

        while (iterator.hasNext()) {
            results.add(iterator.next());
        }

        return results;
    }

}
