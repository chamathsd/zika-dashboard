package org.launchcode.zikaDashboard.controllers.api;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.filter.Filters;
import org.elasticsearch.search.aggregations.bucket.filter.FiltersAggregator;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.pipeline.PipelineAggregatorBuilders;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.models.Report;
import org.launchcode.zikaDashboard.models.resources.ReportResource;
import org.launchcode.zikaDashboard.models.resources.ReportResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value = "/api/report")
public class ReportRestController {

    @Autowired
    ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    ReportResourceAssembler assembler;

    @GetMapping(value = "/{id}")
    public ResponseEntity getReportById(@PathVariable("id") int id) {
        Report report = reportRepository.getOne(id);
        if(report == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        else {
            ReportResource resource = assembler.toResource(report);
            return new ResponseEntity(resource, HttpStatus.OK);
        }
    }

    @GetMapping(value = "/aggs")
    public void getReportAggsByFilter(@RequestParam(defaultValue = "2015-11-28") String start,
                                      @RequestParam(defaultValue = "2018-01-20") String end,
                                      @RequestParam String[] filters) {

        List<FiltersAggregator.KeyedFilter> dataFilters = new ArrayList<>();

        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder()
            .withQuery(QueryBuilders.matchAllQuery())
            .addAggregation(AggregationBuilders.nested("reports", "reports")
                .subAggregation(AggregationBuilders.dateRange("time_period")
                        .field("reports.reportDate")
                        .addRange("time_period", start, end)
                    .subAggregation(AggregationBuilders.reverseNested("locations")
                            .path("")
                        .subAggregation(AggregationBuilders.terms("id")
                                .field("locationId")
                                .size(250)
                            .subAggregation(AggregationBuilders.nested("inner_reports", "reports")
                                .subAggregation(AggregationBuilders.filters("dataFields",
                                        dataFilters.toArray(new FiltersAggregator.KeyedFilter[dataFilters.size()]))
                                    .subAggregation(AggregationBuilders.sum("cases").field("reports.value"))
                                )
                                .subAggregation(PipelineAggregatorBuilders.sumBucket("total", "dataFields>cases"))
                            )
                        )
                    )
                )
            );

        Aggregations aggregations = elasticsearchTemplate.query(searchQuery.build(),
            new ResultsExtractor<Aggregations>() {
                @Override
                public Aggregations extract(SearchResponse response) {
                    return response.getAggregations();
                }
            }
        );

        for (Aggregation agg : aggregations) {
            if (agg.getName().equals("cases")) {
                System.out.println(((Sum) agg).getValue());
            }
        }
    }
}
