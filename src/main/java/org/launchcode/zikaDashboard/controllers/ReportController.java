package org.launchcode.zikaDashboard.controllers;

import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.geom.FeatureCollection;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping(value = "/report")
public class ReportController {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private LocationRepository regionRepository;

    @RequestMapping(value = "/")
    @ResponseBody
    public FeatureCollection getReports() {
        List<Report> reports;
        reports = reportRepository.findAll();

        FeatureCollection result = new FeatureCollection();

//        if (reports.isEmpty()) { return result; }
//
//        for (Report report : reports) {
//            Geometry geom = report.getLocationGeometry();
//            if (geom != null) {
//
//                List<Location> regions = new ArrayList<>();
//                String regionName = "";
//                if (report.getLocationType().equals("country")) {
//                    if (report.getLocation().equals("Brazil") || report.getLocation().equals("Mexico")) {
//                        continue;
//                    } else {
//                        regions = regionRepository.findByName(report.getLocation().replace('_', ' '));
//                    }
//                } else {
//                    String[] locationData = report.getLocation().split("-");
//                    if (locationData.length > 1) {
//                        if (!locationData[0].equals("Brazil") && !locationData[0].equals("Mexico")) {
//                            continue;
//                        } else {
//                            regionName = locationData[1].replace('_', ' ');
//                        }
//                        regions = regionRepository.findByName(regionName);
//                    } else {
//                        continue;
//                    }
//                }
//
//                HashMap<String, Object> properties = new HashMap<>();
//                properties.put("reportDate", report.getReportDate());
//                properties.put("location", report.getLocation());
//                properties.put("locationType", report.getLocationType());
//                properties.put("dataField", report.getDataField());
//                properties.put("dataFieldCode", report.getDataFieldCode());
//                properties.put("value", report.getValue());
//                properties.put("unit", report.getUnit());
//                properties.put("timePeriod", report.getTimePeriod());
//                properties.put("timePeriodType", report.getTimePeriodType());
//                if (regions.size() > 0) {
//                    result.addFeature(new Feature(regions.get(0).getGeometry(), properties));
//                } else {
//                    //result.addFeature(new Feature(report.getLocationGeometry(), properties));
//                }
//            }
//        }

        return result;
    }

}
