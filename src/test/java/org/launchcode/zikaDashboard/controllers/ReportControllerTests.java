package org.launchcode.zikaDashboard.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.IntegrationTestConfig;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.geom.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.closeTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportRepository reportRepository;

    @Test
    public void reportEndpointIsAvailable() throws Exception {
        this.mockMvc.perform(get("/report/")).andExpect(status().isOk());
    }

    @Test
    public void noReportsYieldsEmptyResult() throws Exception {
        this.mockMvc.perform(get("/report/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.type"), containsString("FeatureCollection")))
                .andExpect(jsonPath(("$.features"), hasSize(0)));
    }

//    @Test
//    public void oneReportYieldsOneResult() throws Exception {
//        reportRepository.save(new Report("2016-04-02", "Brazil-Rondonia", "state",
//                "zika_reported", "BR0011" , 618, "cases"));
//        this.mockMvc.perform(get("/report/"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath(("$.features"), hasSize(1)));
//    }
//
//    @Test
//    public void multipleReportsYieldsMultipleReports() throws Exception {
//        reportRepository.save(new Report("2016-04-02", "Brazil-Rondonia", "state",
//                "zika_reported", "BR0011" , 618, "cases"));
//        reportRepository.save(new Report("2016-04-03", "Brazil-Rondonia", "state",
//                "zika_reported", "BR0011" , 720, "cases"));
//        this.mockMvc.perform(get("/report/"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features", hasSize(2)));
//    }
//
//    @Test
//    public void allAttributesAreOnTheGeoJSON() throws Exception {
//        reportRepository.save(new Report("2016-04-02", "Brazil-Rondonia", "state",
//                "zika_reported", "BR0011" , 618, "cases"));
//        this.mockMvc.perform(get("/report/"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features[0].properties.reportDate", equalTo("2016-04-02")))
//                .andExpect(jsonPath("$.features[0].properties.location", containsString("Brazil-Rondonia")))
//                .andExpect(jsonPath("$.features[0].properties.locationType", containsString("state")))
//                .andExpect(jsonPath("$.features[0].properties.dataField", containsString("zika_reported")))
//                .andExpect(jsonPath("$.features[0].properties.dataFieldCode", containsString("BR0011")))
//                .andExpect(jsonPath("$.features[0].properties.value", equalTo(618)))
//                .andExpect(jsonPath("$.features[0].properties.unit", containsString("cases")))
//                .andExpect(jsonPath("$.features[0].geometry.type", equalTo("Point")))
//                .andExpect(jsonPath("$.features[0].geometry.coordinates[0]", closeTo(-62.668875, .00001 )))
//                .andExpect(jsonPath("$.features[0].geometry.coordinates[1]", closeTo(-10.626234, .00001)));
//
//    }
}
