package org.launchcode.zikaDashboard.models;

import org.junit.Test;
import org.launchcode.zikaDashboard.geom.WktHelper;
import static org.junit.Assert.assertEquals;


public class ReportTest {

    @Test
    public void testReportAfterConstruction() {
        Report report = new Report("2016-04-02", "Brazil-Rondonia", "state",
                "zika_reported", "BR0011" , 618, "cases");
        assertEquals("2016-04-02", report.getReportDate());
        assertEquals("Brazil-Rondonia", report.getLocation());
        assertEquals("state", report.getLocationType());
        assertEquals("zika_reported", report.getDataField());
        assertEquals("BR0011", report.getDataFieldCode());
        assertEquals(618, (int) report.getValue());
        assertEquals("cases", report.getUnit());
    }
}
