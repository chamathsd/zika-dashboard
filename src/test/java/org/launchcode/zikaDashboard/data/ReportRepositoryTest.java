package org.launchcode.zikaDashboard.data;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.IntegrationTestConfig;
import org.launchcode.zikaDashboard.geom.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.junit.Assert.assertEquals;

import java.util.List;


@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRepositoryTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportRepository reportRepository;

    @Test
    public void testExpectedNumberOfReports() {
        Report report1 = new Report("a", "a", "a", "a", "a",
                1, "a");
        Report report2 = new Report("a", "a", "a", "a", "a",
                2, "a");
        Report report3 = new Report("a", "a", "a", "a", "a",
                3, "a");
        reportRepository.save(report1);
        reportRepository.save(report2);
        reportRepository.save(report3);
        List<Report> reports = reportRepository.findAll();
        assertEquals(3, reports.size());
    }
}
