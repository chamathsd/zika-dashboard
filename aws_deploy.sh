#!/bin/bash

cd build/libs

# Collapse name to app.jar
mv *.jar app.jar

# Write deployment configuration
cat << EOF > appspec.yml
version: 0.0
os: linux

files:
    - source: ./
      destination: /opt/zika

hooks:
    ApplicationStart:
        - location: run_app.sh
EOF

# Write startup script
cat << EOF > run_app.sh
#!/usr/bin/env bash
echo 'Starting Zika Application'
systemctl stop zika
systemctl disable zika
sleep 10s
systemctl enable zika
systemctl start zika
EOF

zip -r app.zip .

aws s3 cp app.zip s3://$S3_BUCKET

aws deploy create-deployment \
    --application-name chamath-zika-app \
    --deployment-group-name chamath-zika-deployment-group \
    --s3-location bucket=$S3_BUCKET,bundleType=zip,key=app.zip \
    --deployment-config-name CodeDeployDefault.AllAtOnce

